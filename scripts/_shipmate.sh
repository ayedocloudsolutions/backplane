#!/bin/bash
set -e
export CI_BRANCH=$(echo $CI_COMMIT_REF_NAME | sed 's/refs\/heads\///g' | tr _/ -)
export BUILD_BRANCH=${CI_BRANCH:-$(git rev-parse --abbrev-ref HEAD | tr _/ -)}
export BUILD_COMMIT=${CI_COMMIT_SHORT_SHA:-$(git rev-parse --short HEAD)}
export BUILD_VERSION_SUFFIX="build"
export DEFAULT_VERSION="0.0.1"
export LATEST_TAG=$(git describe --abbrev=0 --tags | cut -d'-' -f1 | sed 's/v//g' || echo "$DEFAULT_VERSION")
export LATEST_VERSION=${LATEST_VERSION:-$LATEST_TAG}
export BUILD_VERSION="$LATEST_VERSION-$BUILD_BRANCH.$BUILD_VERSION_SUFFIX-$BUILD_COMMIT"
export DOCKER_BUILD_IMAGE_NAME=${DOCKER_BUILD_IMAGE_NAME:-"$CI_REGISTRY/$CI_PROJECT_PATH"}
export DOCKER_RELEASE_IMAGE_NAME=${DOCKER_RELEASE_IMAGE_NAME:-"$CI_REGISTRY/$CI_PROJECT_PATH"}
export NEXT_VERSION=${NEXT_VERSION:-""}

export DOCKERFILE=${DOCKERFILE:-"$CI_PROJECT_DIR/Dockerfile"}
export DOCKER_CONTEXT=${DOCKER_CONTEXT:-"$CI_PROJECT_DIR"}
export DOCKER_REGISTRY_USERNAME=${DOCKER_REGISTRY_USERNAME:-"gitlab-ci-token"}
export DOCKER_REGISTRY_PASSWORD=${DOCKER_REGISTRY_PASSWORD:-"$CI_JOB_TOKEN"}
export DOCKER_REGISTRY_URL=${DOCKER_REGISTRY_URL:-"$CI_REGISTRY"}
export DOCKER_BUILD_IMAGE_NAME=${DOCKER_BUILD_IMAGE_NAME:-"$CI_REGISTRY/$CI_PROJECT_PATH"}
export DOCKER_RELEASE_IMAGE_NAME=${DOCKER_RELEASE_IMAGE_NAME:-"$CI_REGISTRY/$CI_PROJECT_PATH"}
export DOCKER_HOST=${DOCKER_HOST:-"tcp://docker:2375"}
export DOCKER_TLS_CERTDIR=${DOCKER_TLS_CERT_DIR:-""}
export DOCKER_BUILDKIT=${DOCKER_BUILDKIT:-1}

export PACKAGE_NAME=${PACKAGE_NAME:-"${CI_PROJECT_NAME}"}
export PACKAGE_TYPE=${PACKAGE_TYPE:-"generic"}
export PACKAGE_REGISTRY_PROJECT_ID=${PACKAGE_REGISTRY_PROJECT_ID:-"${CI_PROJECT_ID}"}
export PACKAGE_REGISTRY_TOKEN=${PACKAGE_REGISTRY_TOKEN:-"${CI_JOB_TOKEN}"}
export PACKAGE_REGISTRY_PACKAGE_TYPE=${PACKAGE_REGISTRY_PACKAGE_TYPE:-"${PACKAGE_TYPE}"}


# Load shipment
if [ -f "shipment.env" ]; then
  . shipment.env
else
  echo "shipment.env not found"
fi

# shipmate get latest-version
get-latest-version() {
  echo $LATEST_VERSION
}

# shipmate get build-version
get-build-version() {
  echo $BUILD_VERSION
}

# semantic-release saves the next version to RELEASE_VERSION
# which we will rename to NEXT_VERSION to not have conflicting artifacts in different
# stages our our pipelines
generate-next-version() {
  semantic-release --generate-notes false --branches $BUILD_BRANCH --dry-run
  touch RELEASE_VERSION
  mv RELEASE_VERSION NEXT_VERSION
  cat NEXT_VERSION
}

# shipmate get pre-release-version
get-next-version() {
  # Check if NEXT_VERSION is empty
  if [ -z "$NEXT_VERSION" ]; then
    # Check if file NEXT_VERSION (see generate-next-version) exists
    if [ -f NEXT_VERSION ]; then
      NEXT_VERSION=$(cat NEXT_VERSION)
    else
      echo "File NEXT_VERSION not found"
    fi
  fi
  echo $NEXT_VERSION
}

generate-release-version() {
  semantic-release
}

# shipmate get release-version
get-release-version() {
  if [ -z "$RELEASE_VERSION" ]; then
    if [ -f RELEASE_VERSION ];
    then
      RELEASE_VERSION=$(cat RELEASE_VERSION)
    fi
  fi
  echo $RELEASE_VERSION
}

get-ci-branch() {
  echo $CI_BRANCH
}

get-build-commit() {
  echo $BUILD_COMMIT
}

get-build-version-suffix() {
  echo $BUILD_VERSION_SUFFIX
}

get-default-version() {
  echo $DEFAULT_VERSION
}

get-latest-tag() {
  echo $LATEST_TAG
}

get-dockerfile() {
  echo $DOCKERFILE
}

get-docker-context() {
  echo $DOCKER_CONTEXT
}

get-docker-registry-username() {
  echo $DOCKER_REGISTRY_USERNAME
}

get-docker-registry-password() {
  echo $DOCKER_REGISTRY_PASSWORD
}

get-docker-registry-url() {
  echo $DOCKER_REGISTRY_URL
}

get-docker-host() {
  echo $DOCKER_HOST
}

get-docker-tls-cert-dir() {
  echo $DOCKER_TLS_CERT_DIR
}

get-docker-buildkit() {
  echo $DOCKER_BUILDKIT
}

get-package-name() {
  echo $PACKAGE_NAME
}

get-package-type() {
  echo $PACKAGE_TYPE
}

get-package-registry-project-id() {
  echo $PACKAGE_REGISTRY_PROJECT_ID
}

get-package-registry-token() {
  echo $PACKAGE_REGISTRY_TOKEN
}

get-package-registry-package-type() {
  echo $PACKAGE_REGISTRY_PACKAGE_TYPE
}

# shipmate export [--file shipment.env]
save-shipment() {
  echo "Saving shipment to shipment.env"
  expose-shipment 
  print-shipment > "shipment.env"
}

# not possible in other languages than bash
expose-shipment() {
  export LATEST_VERSION=$(get-latest-version)
  export BUILD_VERSION=$(get-build-version)
  export NEXT_VERSION=$(get-next-version)
  export RELEASE_VERSION=$(get-release-version)

  export CI_BRANCH=$(get-ci-branch)
  export BUILD_BRANCH=$(get-build-branch)
  export BUILD_COMMIT=$(get-build-commit)
  export BUILD_VERSION_SUFFIX=$(get-build-version-suffix)
  export DEFAULT_VERSION=$(get-default-version)
  export LATEST_TAG=$(get-latest-tag)

  export DOCKERFILE=$(get-dockerfile)
  export DOCKER_CONTEXT=$(get-docker-context)
  export DOCKER_REGISTRY_USERNAME=$(get-docker-registry-username)
  export DOCKER_REGISTRY_PASSWORD=$(get-docker-registry-password)
  export DOCKER_REGISTRY_URL=$(get-docker-registry-url)
  export DOCKER_HOST=$(get-docker-host)
  export DOCKER_TLS_CERTDIR=$(get-docker-tls-cert-dir)
  export DOCKER_BUILDKIT=$(get-docker-buildkit)
  export DOCKER_BUILD_IMAGE_NAME=$(get-docker-build-image-name)
  export DOCKER_RELEASE_IMAGE_NAME=$(get-docker-release-image-name)

  export PACKAGE_NAME=$(get-package-name)
  export PACKAGE_TYPE=$(get-package-type)
  export PACKAGE_REGISTRY_TOKEN=$(get-package-registry-token)
  export PACKAGE_REGISTRY_PROJECT_ID=$(get-package-registry-project-id)
  export PACKAGE_REGISTRY_PACKAGE_TYPE=$(get-package-registry-package-type)

}

print-shipment() {
  echo "DEFAULT_VERSION=$DEFAULT_VERSION"
  echo "LATEST_VERSION=$LATEST_VERSION"
  echo "BUILD_VERSION=$BUILD_VERSION"
  echo "NEXT_VERSION=$NEXT_VERSION"
  echo "RELEASE_VERSION=$RELEASE_VERSION"

  echo "CI_BRANCH=$CI_BRANCH"
  echo "BUILD_BRANCH=$BUILD_BRANCH"
  echo "BUILD_COMMIT=$BUILD_COMMIT"
  echo "BUILD_VERSION_SUFFIX=$BUILD_VERSION_SUFFIX"
  echo "LATEST_TAG=$LATEST_TAG"

  echo "DOCKERFILE=$DOCKERFILE"
  echo "DOCKER_CONTEXT=$DOCKER_CONTEXT"
  echo "DOCKER_REGISTRY_URL=$DOCKER_REGISTRY_URL"
  echo "DOCKER_HOST=$DOCKER_HOST"
  echo "DOCKER_TLS_CERTDIR=$DOCKER_TLS_CERTDIR"
  echo "DOCKER_BUILDKIT=$DOCKER_BUILDKIT"
  echo "DOCKER_BUILD_IMAGE_NAME=$DOCKER_BUILD_IMAGE_NAME"
  echo "DOCKER_RELEASE_IMAGE_NAME=$DOCKER_RELEASE_IMAGE_NAME"

  echo "PACKAGE_NAME=$PACKAGE_NAME"
  echo "PACKAGE_TYPE=$PACKAGE_TYPE"
  echo "PACKAGE_REGISTRY_PROJECT_ID=$PACKAGE_REGISTRY_PROJECT_ID"
  echo "PACKAGE_REGISTRY_PACKAGE_TYPE=$PACKAGE_REGISTRY_PACKAGE_TYPE"
}

# shipmate load
# no possible without bash
load-shipment() {
  if [ ! -f "shipment.env" ]; then
    echo "shipment.env not found"
    sleep 5
    return 1
  fi
  echo "Loading shipment from shipment.env"
  source "shipment.env"
  print-shipment
}

get-docker-build-image-name() {
  echo $DOCKER_BUILD_IMAGE_NAME
}

get-docker-release-image-name() {
  echo $DOCKER_RELEASE_IMAGE_NAME
}

# shipmate validate $JOB
check-conditions() {
  JOB=$1
  if [ -z $JOB ];
  then
    echo "Please specify a job to check for: release.code, release.docker.*"
    sleep 5
    return 1
  fi

  echo "Checking conditions for job $JOB"

  if [[ "$JOB" == *"release.code"* ]]; then
    if [[ -z $BUILD_VERSION ]]; then
      echo "BUILD_VERSION not set"
      sleep 5
      exit 0
    fi
    if [[ -z $PRE_RELEASE_VERSION ]]; then
      echo "PRE_RELEASE_VERSION not set" 
      sleep 5
      exit 0
    fi
    if [[ ! -f "CHANGELOG.md" ]]; then
      echo "CHANGELOG.md does not exist. Creating it."
      touch CHANGELOG.md
    fi
  fi

  if [[ "$JOB" == *"release.package"* ]]; then
    if [[ -z $BUILD_VERSION ]]; then
      echo "BUILD_VERSION not set"
      sleep 5
      exit 0
    fi
    if [[ -z $RELEASE_VERSION ]]; then
      echo "RELEASE_VERSION not set"
      sleep 5
      exit 0
    fi
    if [[ "$LATEST_VERSION" == "$RELEASE_VERSION" ]]; then
      echo "RELEASE_VERSION equals LATEST_VERSION"
      sleep 5
      exit 0
    fi
  fi

  if [[ "$JOB" == *"build.docker"* ]]; then
    if [[ -z $BUILD_VERSION ]]; then
      echo "BUILD_VERSION not set"
      sleep 5
      exit 1
    fi
  fi

  if [[ "$JOB" == *"release.docker"* ]]; then
    if [[ -z $BUILD_VERSION ]];  then
      echo "BUILD_VERSION not set"
      sleep 5
      exit 0
    fi
    if [[ -z $RELEASE_VERSION ]]; then
      echo "RELEASE_VERSION not set"
      sleep 5
      exit 0
    fi
    if [[ "$LATEST_VERSION" == "$RELEASE_VERSION" ]]; then
      echo "RELEASE_VERSION equals LATEST_VERSION"
      sleep 5
      exit 0
    fi
  fi
}

# shipmate gitlab mr comment -m "COMMENT" $MR_ID
# shipmate notify gitlab mr --template $JOB
comment-mr() {
  if [ -z $1 ];
  then
    echo "Please specify a job to check for: build.docker"
    sleep 5
    return 1
  fi

  if [[ "$1" == *"build.docker"* ]]; then
    if [[ -n "$CI_MERGE_REQUEST_IID" ]]; then
      cat >/tmp/mrcomment-$CI_MERGE_REQUEST_IID <<EOL
## :whale: new image released

* Image: \`$DOCKER_BUILD_IMAGE_NAME\`
* Version: \`$BUILD_VERSION\`
* Pull: \`docker pull $DOCKER_BUILD_IMAGE_NAME:$BUILD_VERSION\` 
* Run: \`docker run $DOCKER_BUILD_IMAGE_NAME:$BUILD_VERSION\` 
* Commit: $CI_PROJECT_URL/-/commit/$BUILD_COMMIT
EOL
      #NOTE="## :whale: new image released\n\nVersion: \`$BUILD_VERSION\`\nImage URL: \`$DOCKER_IMAGE_NAME:$BUILD_VERSION\`"
      [[ -z $GITLAB_TOKEN ]] && echo "$GL_TOKEN" | glab auth login --stdin
      echo "Updating Merge Request $CI_PROJECT_NAMESPACE/$CI_PROJECT_NAME/!${CI_MERGE_REQUEST_IID}: ${CI_MERGE_REQUEST_TITLE}"
      echo "Check it here: $(glab --repo "$CI_PROJECT_NAMESPACE/$CI_PROJECT_NAME" mr note -m "$(cat /tmp/mrcomment-$CI_MERGE_REQUEST_IID)" $CI_MERGE_REQUEST_IID)"
      rm /tmp/mrcomment-$CI_MERGE_REQUEST_IID
    fi
  fi

  if [[ "$1" == *"build.package"* ]]; then
    if [[ -n "$CI_MERGE_REQUEST_IID" ]]; then
      cat >/tmp/mrcomment-$CI_MERGE_REQUEST_IID <<EOL
## :package: new package released

* Package: \`$PACKAGE_NAME\`
* Version: \`$BUILD_VERSION\`
* Download: \`TBD\` 
EOL
      [[ -z $GITLAB_TOKEN ]] && echo "$GL_TOKEN" | glab auth login --stdin
      echo "Updating Merge Request $CI_PROJECT_NAMESPACE/$CI_PROJECT_NAME/!${CI_MERGE_REQUEST_IID}: ${CI_MERGE_REQUEST_TITLE}"
      echo "Check it here: $(glab --repo "$CI_PROJECT_NAMESPACE/$CI_PROJECT_NAME" mr note -m "$(cat /tmp/mrcomment-$CI_MERGE_REQUEST_IID)" $CI_MERGE_REQUEST_IID)"
      rm /tmp/mrcomment-$CI_MERGE_REQUEST_IID
    fi
  fi
  
}

update-version() {
  PRE_RELEASE_VERSION=${PRE_RELEASE_VERSION:-$(get-pre-release-version)}

  if [ -z $PRE_RELEASE_VERSION ];
  then
    echo "PRE_RELEASE_VERSION NOT SET - not updating files"
    exit 0
  fi

  export RELEASE_VERSION=${RELEASE_VERSION:-$PRE_RELEASE_VERSION}
  export PROJECT_NAME=${CI_PROJECT_NAME:-$(basename $PWD)}

  if [ -d charts/docs ];
  then
    yq e -i '.version = strenv(RELEASE_VERSION)' charts/docs/Chart.yaml
    yq e -i '.appVersion = strenv(RELEASE_VERSION)' charts/docs/Chart.yaml
    yq e -i '.image.tag = strenv(RELEASE_VERSION)' charts/docs/values.yaml
  fi

  if [ -d charts/$PROJECT_NAME ];
  then
    yq e -i '.version = strenv(RELEASE_VERSION)' charts/$PROJECT_NAME/Chart.yaml
    yq e -i '.appVersion = strenv(RELEASE_VERSION)' charts/$PROJECT_NAME/Chart.yaml
    yq e -i '.image.tag = strenv(RELEASE_VERSION)' charts/$PROJECT_NAME/values.yaml
  fi
}

build-docker() {
  docker login $DOCKER_REGISTRY_URL --username=$DOCKER_REGISTRY_USERNAME --password $DOCKER_REGISTRY_PASSWORD
  docker pull $DOCKER_RELEASE_IMAGE_NAME:stable || true
  docker build -f $DOCKERFILE --cache-from $DOCKER_RELEASE_IMAGE_NAME:stable --build-arg APP_BUILD_DATE=$(date -u +'%Y-%m-%dT%H:%M:%SZ') --build-arg APP_BUILD_VERSION=$BUILD_VERSION --build-arg APP_AUTHOR=$CI_PROJECT_NAMESPACE --build-arg APP_NAME=$CI_PROJECT_NAME -t $DOCKER_BUILD_IMAGE_NAME:$BUILD_VERSION $DOCKER_CONTEXT
  docker push $DOCKER_BUILD_IMAGE_NAME:$BUILD_VERSION && echo "Released image $DOCKER_BUILD_IMAGE_NAME:$BUILD_VERSION"
}

release-docker() {
  docker login $DOCKER_REGISTRY_URL --username=$DOCKER_REGISTRY_USERNAME --password $DOCKER_REGISTRY_PASSWORD
  docker pull $DOCKER_BUILD_IMAGE_NAME:$BUILD_VERSION
  docker tag $DOCKER_BUILD_IMAGE_NAME:$BUILD_VERSION $DOCKER_RELEASE_IMAGE_NAME:$RELEASE_VERSION
  docker tag $DOCKER_BUILD_IMAGE_NAME:$BUILD_VERSION $DOCKER_RELEASE_IMAGE_NAME:stable
  docker push $DOCKER_RELEASE_IMAGE_NAME:$RELEASE_VERSION && echo "Releasing image $DOCKER_RELEASE_IMAGE_NAME:$RELEASE_VERSION"
  docker push $DOCKER_RELEASE_IMAGE_NAME:stable && echo "Releasing image $DOCKER_RELEASE_IMAGE_NAME:stable"
}


release.docker() {
  DOCKER_BUILD_IMAGE_NAME=$1
  DOCKER_RELEASE_IMAGE_NAME=$2
  BUILD_VERSION=$3
  RELEASE_VERSION=$4
  DOCKER_REGISTRY_URL=$5
  DOCKER_REGISTRY_USERNAME=$6
  DOCKER_REGISTRY_PASSWORD=$7
  docker login $DOCKER_REGISTRY_URL --username=$DOCKER_REGISTRY_USERNAME --password $DOCKER_REGISTRY_PASSWORD
  docker pull $DOCKER_BUILD_IMAGE_NAME:$BUILD_VERSION
  docker tag $DOCKER_BUILD_IMAGE_NAME:$BUILD_VERSION $DOCKER_RELEASE_IMAGE_NAME:$RELEASE_VERSION
  docker tag $DOCKER_BUILD_IMAGE_NAME:$BUILD_VERSION $DOCKER_RELEASE_IMAGE_NAME:stable
  docker push $DOCKER_RELEASE_IMAGE_NAME:$RELEASE_VERSION
  echo "Releasing image $DOCKER_RELEASE_IMAGE_NAME:$RELEASE_VERSION"
  docker push $DOCKER_RELEASE_IMAGE_NAME:stable
  echo "Releasing image $DOCKER_RELEASE_IMAGE_NAME:stable"
}

build.package() {
  BIN_FILE_NAME_PREFIX=$1
  PROJECT_DIR=$2
  OUTPUT_DIR=$3
  BUILD_VERSION=$4
  #PLATFORMS=$(go tool dist list)
  #PLATFORMS="linux/amd64 linux/386 linux/arm linux/arm64 windows/386 windows/amd64 windows/arm darwin/amd64 darwin/arm64"

  # mkdir -p $GOPATH/src/$REPO_NAME
  # mv $CI_PROJECT_DIR/* $GOPATH/src/$REPO_NAME/
  # cd $GOPATH/src/$REPO_NAME

  PLATFORMS="linux/amd64 linux/arm64 windows/amd64 darwin/amd64 darwin/arm64"
  for PLATFORM in $PLATFORMS; do
    GOOS=${PLATFORM%/*}
    GOARCH=${PLATFORM#*/}
    FILEPATH="$PROJECT_DIR/$OUTPUT_DIR/${GOOS}-${GOARCH}"
    #echo $FILEPATH
    mkdir -p $FILEPATH
    BIN_FILE_NAME="$FILEPATH/${BIN_FILE_NAME_PREFIX}-${GOOS}-${GOARCH}"
    echo $BIN_FILE_NAME
    if [[ "${GOOS}" == "windows" ]]; then BIN_FILE_NAME="${BIN_FILE_NAME}.exe"; fi
    CMD="GOOS=${GOOS} GOARCH=${GOARCH} go build -ldflags=\"-X 'acs/cmd.version=${BUILD_VERSION}'\" -o ${BIN_FILE_NAME}"
    #echo $CMD
    echo "${CMD}"
    eval $CMD || FAILURES="${FAILURES} ${PLATFORM}"
  done
}

# shipmate build package
# shipmate release package --package-name NAME --package-type generic --package-version VERSION --package-registry-project-id ID --package-registry-package-type generic --package-registry-token TOKEN
release.package() {
  PACKAGE_NAME=$1
  PACKAGE_VERSION=$2
  PACKAGE_TYPE=$3
  PACKAGE_REGISTRY_PROJECT_ID=$4
  PACKAGE_REGISTRY_TOKEN=$5
  PACKAGE_REGISTRY_PACKAGE_TYPE=""
  PACKAGE_REGISTRY_BASE="${CI_API_V4_URL}/projects/${PACKAGE_REGISTRY_PROJECT_ID}/packages"
  PACKAGE_REGISTRY_TOKEN_TYPE="JOB-TOKEN"

  echo "PACKAGE_NAME=$PACKAGE_NAME"
  echo "PACKAGE_TYPE=$PACKAGE_TYPE"
  echo "PACKAGE_VERSION=$PACKAGE_VERSION"
  echo "PACKAGE_REGISTRY_BASE=$PACKAGE_REGISTRY_BASE"

  if [ "$PACKAGE_TYPE" == "go" ]; then
    PACKAGE_REGISTRY_PACKAGE_TYPE="generic"
  fi
  echo "PACKAGE_REGISTRY_PACKAGE_TYPE=$PACKAGE_REGISTRY_PACKAGE_TYPE"
  if [ "$PACKAGE_REGISTRY_PROJECT_ID" != "$CI_PROJECT_ID" ]; then
    PACKAGE_REGISTRY_TOKEN_TYPE="PRIVATE-TOKEN"
  fi
  echo "PACKAGE_REGISTRY_TOKEN_TYPE=$PACKAGE_REGISTRY_TOKEN_TYPE"

  PACKAGE_REGISTRY_URL="$PACKAGE_REGISTRY_BASE/${PACKAGE_REGISTRY_PACKAGE_TYPE:-$PACKAGE_TYPE}/${PACKAGE_NAME}/${PACKAGE_VERSION}"
  for file in `find packages/$PACKAGE_TYPE -type f`; do
    echo "Uploading $file to ${PACKAGE_REGISTRY_URL}"
    curl --header "$PACKAGE_REGISTRY_TOKEN_TYPE: ${PACKAGE_REGISTRY_TOKEN}" --upload-file $file ${PACKAGE_REGISTRY_URL}/$(basename $file)
  done
}